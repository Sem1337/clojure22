(ns getting-started.core
  (:gen-class))

(defn -main [& args]
  (println "Working!"))



(defn produceNewWords_tailcall [word alphabet acc]
  (if (empty? alphabet)
    acc
    (if (= (first word) (first alphabet))
      (produceNewWords_tailcall word (rest alphabet) acc)
      (produceNewWords_tailcall word (rest alphabet) (cons (cons (first alphabet) word)  acc)))))

(defn extendWords_tailcall [words alphabet acc]

  (if (empty? words)
    acc
    (extendWords_tailcall (rest words) alphabet (concat acc (produceNewWords_tailcall (first words) alphabet (list))))))


(defn getAnswer_tailcall 
  ([alphabet n acc]
   (if (= n 0)
     acc
     (getAnswer_tailcall alphabet (- n 1) (extendWords_tailcall acc alphabet (list)))
    ))

  ( [alphabet n] 
   (getAnswer_tailcall alphabet n (list (list)))
   )
 )


(defn produceNewWords [word alphabet]
  (if (empty? alphabet)
    (list)
    (if (= (first word) (first alphabet))
      (produceNewWords word (rest alphabet))
      (cons (cons (first alphabet) word) (produceNewWords word (rest alphabet))))))

(defn extendWords [words alphabet]

  (if (empty? words)
    (list)
    (concat (produceNewWords (first words) alphabet) (extendWords (rest words) alphabet))))

(defn getAnswer [alphabet n]
  (if (= n 0)
    (list (list))
    (extendWords (getAnswer alphabet (- n 1)) alphabet)))




;; produce new collection applying function f to each element of given collection
(defn my_map [f coll]
  (seq (reduce #(conj %1 (f %2)) [] coll)))

;; get new collection  with elements satisfying the predicate
(defn my_filter [pred coll]
  (reduce #(if (pred %2) (seq (conj %1 %2)) (seq %1)) [] coll))

(declare getWords)

;; get list of words of length n made up of given objects, with provided first object in each word 
(defn getWordsWithFirstFixed [list1 n elem]
  (if (> n 1)
    (my_filter #(not= (second %1) elem) (my_map #(cons elem %1) (getWords list1 (- n 1))))
    (list elem)))

;; get list of words of length n made up of given objects. There are no two same objects next to each other
(defn getWords [list1 n]
  (if (> n 1)
    (reduce #(concat %1 %2) (my_map #(getWordsWithFirstFixed list1 n %1) list1))
    (my_map list list1)))


;; utility funcs
(defn print_result [ansList initList n name]
  (println name)
  (println "list: " initList " words of length " n)
  (run! println ansList)
  (println (count ansList) " words in total\n"))

(defn run_test [list1 n]
  (print_result (getWords list1 n) list1 n "first solution")
  (print_result (getAnswer list1 n) list1 n "1.1 solution")
  (print_result (getAnswer_tailcall list1 n) list1 n "1.2 solution")
  )


(run_test '(1 2) 2)
(run_test '(1 2) 3)
(run_test '(1 2) 5)
(run_test '(1 2 3) 1)
(run_test '(1 2 3) 2)
(run_test '(1 2 3) 3)
(run_test '(1 2 3) 4)
(run_test '((1 2) (3 4) (5 6)) 3)
(run_test '((1 2 3) (4 5) (6 7)) 2)
